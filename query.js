'use strict'

const client = require('./lib/client')

function query (expressions) {
  let expressionString = expressions // assume semi-colon delimited string
  if (Array.isArray(expressions)) expressionString = expressions.join('; ')

  return client('query', { method: 'POST', body: { query: expressionString } }).then(result => result.data)
}

module.exports = query

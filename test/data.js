'use strict'

const { data } = require('../')

const { test } = require('tap')
const nock = require('nock')

test('understands HTTP formatted policy', async ({ is, ok }) => {
  const opa = nock('http://localhost:8181')
    .post('/v1/data/opa/examples/allow_request', { input: { example: { flag: true } } })
    .twice()
    .reply(200, { result: true })

  const { result: plain } = await data('opa/examples/allow_request', { example: { flag: true } })
  is(plain, true)

  const { result: leadingSlash } = await data('/opa/examples/allow_request', { example: { flag: true } })
  is(leadingSlash, true)

  ok(opa.isDone())
})

test('understands Rego policy name', async ({ is, ok }) => {
  const opa = nock('http://localhost:8181')
    .post('/v1/data/opa/examples/allow_request', { input: { example: { flag: true } } })
    .reply(200, { result: true })

  const { result: rego } = await data('opa.examples.allow_request', { example: { flag: true } })
  is(rego, true)

  ok(opa.isDone())
})

test('accepts a YAML input', async ({ is, ok }) => {
  const yamlInput = `
    example:
      flag: true`

  const opa = nock('http://localhost:8181', { reqHeaders: { 'Content-Type': 'application/x-yaml' } })
    .post('/v1/data/opa/examples/allow_request', `input:\n  ${yamlInput}`)
    .reply(200, { result: true })

  const { result: yaml } = await data('opa.examples.allow_request', yamlInput)
  is(yaml, true)

  ok(opa.isDone())
})

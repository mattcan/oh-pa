'use strict'

const { query } = require('../')
const { OpaServerError } = require('../lib/errors')

const { test } = require('tap')
const nock = require('nock')

test('works with one expression', async ({ is, match }) => {
  const scope = nock('http://localhost:8181')
    .post('/v1/query', { query: 'x = data.example.allow' })
    .reply(200, { result: [{ x: true }] })

  const { result } = await query('x = data.example.allow')

  is(result.length, 1)
  match(result[0], { x: true })

  is(scope.isDone(), true)
})

test('works with array of expressions', async ({ is, match }) => {
  const scope = nock('http://localhost:8181')
    .post('/v1/query', { query: 'x = data.example.allow; b = x == true' })
    .reply(200, { result: [{ b: true, x: true }] })

  const { result } = await query(['x = data.example.allow', 'b = x == true'])
  is(result.length, 1)
  match(result[0], { b: true, x: true })

  is(scope.isDone(), true)
})

test('fails when query is empty', async ({ is, rejects }) => {
  const scope = nock('http://localhost:8181')
    .post('/v1/query', { query: '' })
    .reply(500, { code: 'internal_error', message: 'cannot evaluate empty query' })

  await rejects(query(''), OpaServerError)

  is(scope.isDone(), true)
})

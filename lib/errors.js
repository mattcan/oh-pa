'use strict'

class OhpaError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

class OpaBadRequest extends OhpaError {
  constructor (error) {
    super(`Bad request: ${error.message}`)
    this.statusCode = 400
  }
}

class OpaServerError extends OhpaError {
  constructor (error) {
    super(`Internal server error: ${error.message}`)
    this.statusCode = 500
  }
}

class OpaStreamingNotImplemented extends OhpaError {
  constructor () {
    super('Streaming not implemented in OPA')
    this.statusCode = 501
  }
}

class SdkUsageError extends OhpaError {
  constructor (error) {
    super('Incorrect usage of SDK')
    this.detail = error
  }
}

module.exports = {
  OpaBadRequest,
  OpaServerError,
  OpaStreamingNotImplemented,
  SdkUsageError
}

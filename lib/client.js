'use strict'

const http = require('http')
const https = require('https')

const { OpaServerError, OpaStreamingNotImplemented, OpaBadRequest } = require('./errors')

const _defaults = {
  host: 'http://localhost:8181',
  method: 'GET',
  version: 'v1',
  body: null,
  isYaml: false
}

function client (path, options = {}) {
  const opts = { ..._defaults, ...options }
  const c = opts.host.startsWith('https') ? https : http

  return new Promise((resolve, reject) => {
    const url = `${opts.host}/${opts.version}/${path}`
    const headers = {
      'Content-Type': opts.isYaml ? 'application/x-yaml' : 'application/json'
    }
    const request = c.request(url, { method: opts.method, headers }, (res) => {
      const data = []
      res.on('data', chunk => data.push(chunk))
      res.on('end', () => {
        const body = JSON.parse(data.join(''))

        if (res.statusCode < 200 || res.statusCode > 399) {
          let error
          switch (res.statusCode) {
            case 400: error = new OpaBadRequest(body); break
            case 501: error = new OpaStreamingNotImplemented(); break
            default: error = new OpaServerError(body)
          }

          reject(error)
        }

        resolve({ data: body })
      })
    })

    request.on('error', err => reject(err))

    if (['PUT', 'PATCH', 'POST'].includes(opts.method.toUpperCase())) {
      let body = JSON.stringify(opts.body)
      if (opts.isYaml) body = opts.body

      request.write(body)
    }

    request.end()
  })
}

module.exports = client

# Roadmap

This is a rough plan

## 0.3.0

Simplified interface for decision making. The goal is to make this usable for
developers who don't care to learn about the intricacies of OPA.

## 0.4.0

A management interface for creating and updating both data and policies. This
enables teams to create policies specific for their application and have them
loaded into OPA at runtime or during CI/CD.

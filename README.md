# oh-pa

A Nodejs library for interacting with the [Open Policy Agent (OPA)][opa] REST
API.

This project is experimental and there will likely be major breaking changes
until we reach 1.0. Take a look at the [ROADMAP.md](ROADMAP.md) to get a sense
of what changes are coming.

[opa]: https://www.openpolicyagent.org

## Why

I wanted to build a zero dependency package that simplifies access to the OPA
REST API.

## Install

```sh
npm install --save oh-pa
```

## Usage

### `query` - for adhoc policy decisions

Need a decision made with data that OPA has but don't need/want/have a named policy? This
is the function to use.

```js
'use strict'

const { query } = require('oh-pa')

query('x = data.example.allow')
  .then(console.log)
  .catch(console.error)
```

### `data` - for named policy decisions

Use named policies to make decisions using stored data and/or input.

```js
'use strict'

const { data } = require('oh-pa')

data('opa.examples.allow_request', { example: { flag: true } })
// NOTE: you can also use slashes instead of periods - opa/examples/allow_request
  .then(console.log)
  .catch(console.error)
```

'use strict'

module.exports = {
  query: require('./query'),
  data: require('./data')
}

'use strict'

const client = require('./lib/client')

function data (policy, input = {}) {
  let formattedPolicy = policy.replace(/\./g, '/')
  if (formattedPolicy[0] === '/') formattedPolicy = formattedPolicy.slice(1)

  const isYaml = typeof input === 'string'

  return client(`data/${formattedPolicy}`, { method: 'POST', body: isYaml ? `input:\n  ${input}` : { input }, isYaml }).then(result => result.data)
}

module.exports = data
